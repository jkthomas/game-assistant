
import cv2 as cv
import time
from tools.window_capture import capture_screenshot
from tools.detector import is_detected
from tools.inputter import click_at_coords

width = 1920
height = 1080

# 55 pixels difference
c_skill_coords = (1037.5, 1031.5)
q_skill_coords = (1092.5, 1031.5)
z_skill_coords = (1147.5, 1031.5)
e_skill_coords = (1202.5, 1031.5)
r_skill_coords = (1257.5, 1031.5)

time.sleep(2)

while (True):
    screenshot = capture_screenshot(width, height)

    utility_skills_bar = screenshot[1000:1080, 1000:1300]
    if is_detected(utility_skills_bar, 'base_assist/base_assist_icons/healing_turret_icon.jpg', 0.95) == True:
        print('Healing turret activation...')
        click_at_coords(c_skill_coords, width, height)
    elif is_detected(utility_skills_bar, 'base_assist/base_assist_icons/flame_turret_icon.jpg', 0.95) == True:
        print('Flame turret activation...')
        click_at_coords(q_skill_coords, width, height)
    elif is_detected(utility_skills_bar, 'base_assist/base_assist_icons/rocket_turret_icon.jpg', 0.95) == True:
        print('Rocket turret activation...')
        click_at_coords(z_skill_coords, width, height)
    elif is_detected(utility_skills_bar, 'base_assist/base_assist_icons/riffle_turret_icon.jpg', 0.95) == True:
        print('Riffle turret activation...')
        click_at_coords(e_skill_coords, width, height)
    elif is_detected(utility_skills_bar, 'base_assist/base_assist_icons/supply_crate_icon.jpg', 0.95) == True:
        print('Supply crate activation...')
        click_at_coords(r_skill_coords, width, height)

    if cv.waitKey(1) == ord('q'):
        cv.destroyAllWindows()
        break

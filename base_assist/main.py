
import cv2 as cv
import time
from tools.window_capture import capture_screenshot
from tools.detector import draw_detection
from tools.inputter import press_button
from tools.computer_vision import show_computer_vision, show_text_on_image

width = 1920
height = 1080

# 55 pixels difference
c_skill_coords = (1037.5, 1031.5)
q_skill_coords = (1092.5, 1031.5)
z_skill_coords = (1147.5, 1031.5)
e_skill_coords = (1202.5, 1031.5)
r_skill_coords = (1257.5, 1031.5)

time.sleep(2)
loop_time = time.time()

while (True):
    # screenshot = capture_screenshot(width, height)

    element_to_find_image = cv.imread(
        'base_assist/farm_assist_icons/wvw1.jpg', cv.IMREAD_UNCHANGED)
    screenshot_part = element_to_find_image[:, 1550:1920]
    draw_detection(screenshot_part,
                   'base_assist/farm_assist_icons/wvw_reward_icon.jpg', 0.9)

    draw_detection(screenshot_part,
                   'base_assist/farm_assist_icons/mist_war_window_title.jpg', 0.9)

    loop_time = time.time()
    # TODO: Doesn't work, fix
    # show_text_on_image(screenshot, 'FPS {}'.format(
    #     1 / (time.time() - loop_time)))
    show_computer_vision(screenshot_part)

    if cv.waitKey(1) == ord('q'):
        cv.destroyAllWindows()
        break

# https://github.com/learncodebygaming/opencv_tutorials/blob/master/009_assist/windowcapture.py
# https://www.youtube.com/watch?v=7k4j-uL8WSQ&list=PL1m2M8LQlzfKtkKq2lK5xko4X-8EZzFPI&index=5

import cv2 as cv
import time
from tools.window_capture import capture_screenshot
from tools.detector import is_detected, get_is_detected_coords
from tools.inputter import click_at, click_at_coords
from threading import Thread


width = 1920
height = 1080

wvw_icon_coords = (209.0, 16.0)
script_launch_time_margin = 1
post_launch_prep_time = 2
map_swap_time_margin = 25
reward_acquisition_time_margin = 15


time.sleep(post_launch_prep_time)


def get_ribbon_image(ribbon_color):
    return f'base_assist/farm_assist_icons/{ribbon_color}_map_ribbon.jpg'


def click_yes_button(screenshot):
    is_yes_button_detected, yes_button_coords = get_is_detected_coords(
        screenshot, 'base_assist/farm_assist_icons/yes_button.jpg', 0.9)
    if is_yes_button_detected == True:  # Yes button is visible, so clicked on it
        click_at_coords(yes_button_coords, width, height)
        print('Confirmation (yes) button clicked...')
        time.sleep(0.5)
        return True
    else:
        print('Confirmation (yes) button NOT detected. Returning...')
        return False


def is_interface_loaded(top_left_corner_screenshot):
    return is_detected(
        top_left_corner_screenshot, 'base_assist/farm_assist_icons/options_icon.jpg', 0.9)


def open_window_and_change_to_maps_tab():
    while (True):
        screenshot = capture_screenshot(width, height)

        if is_interface_loaded(screenshot[0:50, 0:50]) == True:
            is_maps_tab_detected = is_detected(
                screenshot, f'base_assist/farm_assist_icons/maps_tab.jpg', 0.9)
            if is_maps_tab_detected == True:
                break
            else:
                is_wvw_window_title_detected, wvw_window_title_coords = get_is_detected_coords(
                    screenshot, 'base_assist/farm_assist_icons/wvw_window_title.jpg', 0.9)
                if is_wvw_window_title_detected == False:
                    click_at_coords(wvw_icon_coords, width, height)
                    time.sleep(0.5)
                    continue
                else:
                    map_tab_coords = (int(wvw_window_title_coords[0]) - 120, int(
                        wvw_window_title_coords[1]) + 70)  # Offsets for the button relative to the wvw_window_title
                    click_at_coords(map_tab_coords, width, height)
                    time.sleep(0.5)
                    break


def is_already_in_wvw_map():
    while (True):
        screenshot = capture_screenshot(width, height)

        if is_interface_loaded(screenshot[0:50, 0:50]) == True:
            is_leave_icon_detected = is_detected(
                screenshot, 'base_assist/farm_assist_icons/leave_icon.jpg', 0.9)
            if is_leave_icon_detected == True:  # This means that the character is at WvW map and a window is already open
                return True
            else:
                is_maps_tab_detected = is_detected(
                    screenshot, f'base_assist/farm_assist_icons/maps_tab.jpg', 0.9)
                if is_maps_tab_detected == True:
                    return False
                else:
                    is_wvw_window_title_detected, wvw_window_title_coords = get_is_detected_coords(
                        screenshot, 'base_assist/farm_assist_icons/wvw_window_title.jpg', 0.9)
                    if is_wvw_window_title_detected == False:
                        click_at_coords(wvw_icon_coords, width, height)
                        time.sleep(0.5)
                        continue
                    else:
                        map_tab_coords = (int(wvw_window_title_coords[0]) - 120, int(
                            wvw_window_title_coords[1]) + 70)  # Offsets for the button relative to the wvw_window_title
                        click_at_coords(map_tab_coords, width, height)
                        time.sleep(0.5)
                        continue


def change_wvw_map(first_ribbon_color, second_ribbon_color):
    while (True):
        screenshot = capture_screenshot(width, height)

    # TODO: When detecting ribbons, don't check whole screen, it takes too much time - check only wvw window by using title coords
        if is_interface_loaded(screenshot[0:50, 0:50]) == True:
            is_first_map_ribbon_detected, first_map_ribbon_coords = get_is_detected_coords(
                screenshot, get_ribbon_image(first_ribbon_color), 0.95)
            if is_first_map_ribbon_detected == True:
                click_at(
                    first_map_ribbon_coords[0] + 100, first_map_ribbon_coords[1], width, height)
                click_yes_button(capture_screenshot(width, height))
                return
            else:
                is_second_map_ribbon_detected, second_map_ribbon_coords = get_is_detected_coords(
                    screenshot, get_ribbon_image(second_ribbon_color), 0.95)
                if is_second_map_ribbon_detected == True:
                    click_at(
                        second_map_ribbon_coords[0] + 100, second_map_ribbon_coords[1], width, height)
                    click_yes_button(capture_screenshot(width, height))
                    return
                else:
                    print('ERROR: NONE OF THE RIBBONS FOUND WHEN CHANGING MAP')
                    return


def leave_wvw_map():
    if is_already_in_wvw_map() == True:  # This already opens the window and sets tab
        screenshot = capture_screenshot(width, height)
        if is_interface_loaded(screenshot[0:50, 0:50]) == True:
            is_leave_icon_detected, leave_icon_coords = get_is_detected_coords(
                screenshot, 'base_assist/farm_assist_icons/leave_icon.jpg', 0.9)
            if is_leave_icon_detected == True:  # This means that the character is at WvW map and a window is already open
                click_at_coords(leave_icon_coords, width, height)
                print('Leave button clicked...')
                click_yes_button(capture_screenshot(width, height))
                return
            else:
                print('ERROR: INSIDE THE MAP, BUT NO LEAVE ICON DETECTED')
                return


def do_maps_double_swap(first_ribbon_color, second_ribbon_color):
    if is_already_in_wvw_map() == True:
        change_wvw_map(first_ribbon_color, second_ribbon_color)
    else:
        change_wvw_map(first_ribbon_color, second_ribbon_color)
        open_window_and_change_to_maps_tab()
        change_wvw_map(first_ribbon_color, second_ribbon_color)


# TODO: Add a safety system if the cycle misses the reward - it will stay for a whole 4mins50secs now until the next reward
def wait_for_reward(start_countdown_callback):
    is_cycle_finished = False
    while (True):
        screenshot = capture_screenshot(width, height)

        if is_interface_loaded(screenshot[0:50, 0:50]) == True:
            is_cycle_finish_icon_detected = is_detected(
                screenshot, f'base_assist/farm_assist_icons/cycle_finish.jpg', 0.9)
            if is_cycle_finished == False and is_cycle_finish_icon_detected == True:
                print('Start countdown callback starting...')
                start_countdown_callback()
                print('CYCLE FINISHED, UPDATING FLAG')
                is_cycle_finished = True
            elif is_cycle_finished == True and is_cycle_finish_icon_detected == False:
                break


def do_reward_cycle(ribbons, start_countdown_callback):
    do_maps_double_swap(ribbons[0], ribbons[1])
    wait_for_reward(start_countdown_callback)
    leave_wvw_map()


# TODO: Add all 3 ribbons and cycle between them when there are queues - add a functionality to detect queues
maps_ribbons_colors_param = ['red', 'green']


class CustomThread(Thread):
    def __init__(self, interval):
        Thread.__init__(self)
        self.interval = interval
        self.pauseFlag = True

    def start_countdown(self):
        self.pauseFlag = False

    # TODO: The timer becomes longer and longer ('run' is called after an increasing time every call)
    # Fix by setting time instead of pauseFlag and set interval based on time difference in the 'run' call OR just research the topic more
    # TODO: Research injecting the do_reward_cycle instead of calling it explicitly
    def run(self):
        while True:
            if self.pauseFlag == False:
                self.pauseFlag = True
                print(f'Countdown started with interval of {self.interval}')
                time.sleep(self.interval)
                do_reward_cycle(maps_ribbons_colors_param,
                                self.start_countdown)


reward_timer_thread = CustomThread(255)
reward_timer_thread.start()


# Input params
time_to_first_reward = '01:00'
# END of input params

# TODO: Add more logs for easier phase recognition
# TODO: Add moving the window into the middle of the screen to avoid covering cycle timer at the top with the window or window being to low and missing map ribbons/leave icon
# Doable with WvW title detection coords with drag to the specific coords
time.sleep(120)
do_reward_cycle(maps_ribbons_colors_param, reward_timer_thread.start_countdown)

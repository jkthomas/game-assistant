import pydirectinput


def press_button(button):
    pydirectinput.press(button)


def click_at(x, y, width, height):
    pydirectinput.leftClick(int(x), int(y))
    pydirectinput.moveTo(int(width / 2), int(height / 2))


def click_at_coords(coords, width, height):
    pydirectinput.leftClick(int(coords[0]), int(coords[1]))
    pydirectinput.moveTo(int(width / 2), int(height / 2))

import cv2 as cv


def is_detected(main_image, match_image_path, threshold):
    element_to_find_image = cv.imread(match_image_path, cv.IMREAD_UNCHANGED)

    result = cv.matchTemplate(
        main_image, element_to_find_image, cv.TM_CCOEFF_NORMED)

    _min_val, max_match_percentage_value, _min_loc, _max_match_location_coords = cv.minMaxLoc(
        result)

    if max_match_percentage_value >= threshold:
        return True
    else:
        return False


def get_is_detected_coords(main_image, match_image_path, threshold):
    element_to_find_image = cv.imread(match_image_path, cv.IMREAD_UNCHANGED)

    result = cv.matchTemplate(
        main_image, element_to_find_image, cv.TM_CCOEFF_NORMED)

    _min_val, max_match_percentage_value, _min_loc, max_match_location_coords = cv.minMaxLoc(
        result)

    if max_match_percentage_value >= threshold:
        element_to_find_image_width = element_to_find_image.shape[1]
        element_to_find_image_height = element_to_find_image.shape[0]

        top_left_coords = max_match_location_coords
        middle_coords = (
            top_left_coords[0] + element_to_find_image_width/2, top_left_coords[1] + element_to_find_image_height/2)

        return True, middle_coords
    else:
        return False, (0, 0)


def draw_detection(main_image, match_image_path, threshold):
    element_to_find_image = cv.imread(match_image_path, cv.IMREAD_UNCHANGED)

    result = cv.matchTemplate(
        main_image, element_to_find_image, cv.TM_CCOEFF_NORMED)

    _min_val, max_match_percentage_value, _min_loc, max_match_location_coords = cv.minMaxLoc(
        result)

    print(max_match_percentage_value)
    if max_match_percentage_value >= threshold:
        turret_image_width = element_to_find_image.shape[1]
        turret_image_height = element_to_find_image.shape[0]

        top_left_coords = max_match_location_coords
        bottom_right_coords = (
            top_left_coords[0] + turret_image_width, top_left_coords[1] + turret_image_height)

        cv.rectangle(main_image, top_left_coords, bottom_right_coords,
                     color=(0, 255, 0), thickness=2, lineType=cv.LINE_4)

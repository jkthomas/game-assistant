import cv2 as cv


def show_text_on_image(image, text):
    font = cv.FONT_HERSHEY_SIMPLEX
    org = (50, 50)
    fontScale = 1
    color = (255, 0, 0)
    thickness = 2
    image = cv.putText(image, text, org, font,
                       fontScale, color, thickness, cv.LINE_AA)


def show_computer_vision(screenshot):
    cv.imshow('Computer Vision', screenshot)
